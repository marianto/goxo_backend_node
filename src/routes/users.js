const express = require('express');
const jwt = require('jsonwebtoken');
const authenticateJWT = require('../middlewares/authentication');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const Users = require('../models/Users');

const router = express.Router();

router.get('/', authenticateJWT, (req, res) => {
	Users.find({})
		.then((user) => {
			res.send(user);
		})
		.catch((error) => {
			res.status(500).send(error);
		})
});

router.post('/', (req, res) => {

	console.log(req.body);

	const name = req.body.name
	const email = req.body.email
	const city = req.body.city
	const password = req.body.password

	bcrypt.hash(password, saltRounds, function (err, hash) {

		const user = new Users()

		user.name = name;
		user.email = email;
		user.city = city;
		user.password = hash;

		user.save()
			.then((newUser) => {
				const accessToken = jwt.sign(
					{ userID: newUser.id, name: newUser.name },
					process.env.JWT_SECRET);
				return res.json({ logged: true, token: accessToken, user: newUser })
			})
			.catch((err) => {
				res.status(500).send(err);
			})
	});
});

router.post('/login', (req, res) => {
	const email = req.body.email;
	const password = req.body.password;

	Users.findOne({ email: email })
		.then((user) => {
			if (user) {
				bcrypt.compare(password, user.password, function (err, result) {
					if (result) {
						const accessToken = jwt.sign(
							{ userID: user.id, name: user.name },
							process.env.JWT_SECRET);
						return res.json({ logged: true, token: accessToken })
					}
					else {
						return res.status(404).json({ logged: false })
					}
				});
			}
			else {
				return res.status(404).json({ logged: false })
			}
		})
		.catch((err) => {
			return res.status(404).json({ logged: false })
		})
});




router.get('/singleUser', authenticateJWT, (req, res) => {
	const id = req.user.userID;
	Users.findById(id, { __v: 0, updatedAt: 0, createdAt: 0 })
		.then((user) => {
			res.send(user)
		})
		.catch((error) => {
			res.status(500).send(error)
		})
});



module.exports = router;

const express = require('express');
const usersRoutes = require('./routes/users');
const cors = require('cors');

require('dotenv').config();
require('./db.js');

const PORT = process.env.PORT;
const server = express();
server.use(express.static('public'));
server.use(cors());

server.use(express.json());
server.use(express.urlencoded({extended: false}));

server.use('/users', usersRoutes);


server.listen(PORT, () => {
    console.log(`Server running in http://localhost:${PORT}`);
});